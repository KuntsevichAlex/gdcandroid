package com.gdcandroid.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.gdcandroid.model.ListViewItem;

import java.util.ArrayList;
import java.util.List;

public class DatabaseService {

    private static DatabaseService instance;
    private SQLiteDatabase database;

    private static final String DATABASE_NAME = "App";
    private static final String TABLE_DDL = "CREATE TABLE IF NOT EXISTS ListItem (Path VARCHAR, Text VARCHAR);";

    public static DatabaseService getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseService(context);
        }
        return instance;
    }

    private DatabaseService(Context context) {
        database = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
        database.execSQL(TABLE_DDL);
    }

    public void persistItem(ListViewItem item) {
        String sql = "insert into ListItem values ('" + item.getImage() + "','" + item.getText() + "');";
        database.execSQL(sql);
    }

    public List<ListViewItem> findAll() {
        String sql = "select * from ListItem";
        Cursor c = database.rawQuery(sql, null);
        List<ListViewItem> list = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                String column1 = c.getString(0);
                String column2 = c.getString(1);
                ListViewItem item = new ListViewItem();
                item.setImage(column1);
                item.setText(column2);
                list.add(item);
            } while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public void delete(String text) {
        String sql = "delete from ListItem where Text='" + text + "'";
        database.execSQL(sql);
    }

    public void deleteAll() {
        String sql = "delete from ListItem;";
        database.execSQL(sql);
    }

}
