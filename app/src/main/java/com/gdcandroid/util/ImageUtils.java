package com.gdcandroid.util;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImageUtils {

    private File directory;
    private static ImageUtils instance;

    public static ImageUtils getInstance(Context context) {
        if (instance == null) {
            instance = new ImageUtils(context);
        }
        return instance;
    }

    private ImageUtils(Context context) {
        ContextWrapper cw = new ContextWrapper(context);
        directory = cw.getDir("app", Context.MODE_PRIVATE);
    }

    public String saveToInternalStorage(Bitmap bitmapImage, String fileName) {
        File myPath = new File(directory, fileName);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(myPath);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return myPath.getAbsolutePath();
    }

    public List<Bitmap> loadImagesFromStorage() {
        List<Bitmap> list = new ArrayList<>();
        for (File file : directory.listFiles())
        try {
            list.add(BitmapFactory.decodeStream(new FileInputStream(file)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Bitmap loadImage(String path) {
        try {
            return BitmapFactory.decodeStream(new FileInputStream(new File(path)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteFile(String path) {
        File file = new File(path);
        file.delete();
    }

    public void clearDirectory() {
        for (File file : directory.listFiles()) {
            file.delete();
        }
    }

}
