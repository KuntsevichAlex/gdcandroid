package com.gdcandroid;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.gdcandroid.adapter.CustomAdapter;
import com.gdcandroid.database.DatabaseService;
import com.gdcandroid.model.ListViewItem;
import com.gdcandroid.util.ImageUtils;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ImageUtils imageUtils;
    DatabaseService databaseService;
    ArrayAdapter<ListViewItem> adapter;

    public List<Bitmap> images = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button scan = (Button) findViewById(R.id.scanBtn);
        imageUtils = ImageUtils.getInstance(this);
        databaseService = DatabaseService.getInstance(this);
        List<ListViewItem> items = databaseService.findAll();
        listView = (ListView) findViewById(R.id.listView);
        TextView emptyText = (TextView)findViewById(android.R.id.empty);
        listView.setEmptyView(emptyText);
        adapter = new CustomAdapter(this, R.layout.list_item, items);
        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                ListViewItem item = adapter.getItem(position);
                adapter.remove(item);
                databaseService.delete(item.getText());
                imageUtils.deleteFile(item.getImage());
                return false;
            }
        });
        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                integrator.setPrompt("");
                List<String> onDesiredFormats = Arrays.asList("EAN_13", "PDF_417");
                integrator.initiateScan(onDesiredFormats);
            }
        });
    }

    private void deleteAllStuff() {
        databaseService.deleteAll();
        imageUtils.clearDirectory();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            String re = scanResult.getContents();
            if (re != null) {
                String url = null;
                if (!re.startsWith("46")) {
                    url = "https://cfcdnpull-creativefreedoml.netdna-ssl.com/wp-content/uploads/2013/03/00-android-4-0_icons.png";
                } else {
                    url = "http://www.dk0tu.de/index/RU.png";
                }
                new DownloadImageTask(this, re).execute(url);
            }
            adapter.notifyDataSetChanged();
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        private ProgressDialog dialog;
        private String code;

        public DownloadImageTask(Context context, String code) {
            dialog = new ProgressDialog(context);
            this.code = code;
        }

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Please wait");
            this.dialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            String urlDisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urlDisplay).openStream();
                mIcon11 = Bitmap.createScaledBitmap(BitmapFactory.decodeStream(in), 48, 48, false);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            String path = imageUtils.saveToInternalStorage(result, code);
            ListViewItem item = new ListViewItem();
            item.setImage(path);
            item.setText(code);
            databaseService.persistItem(item);
            adapter.add(item);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }
}
