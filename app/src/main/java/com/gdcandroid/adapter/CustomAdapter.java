package com.gdcandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gdcandroid.MainActivity;
import com.gdcandroid.R;
import com.gdcandroid.model.ListViewItem;
import com.gdcandroid.util.ImageUtils;

import java.util.List;

public class CustomAdapter extends ArrayAdapter<ListViewItem> {

    private List<ListViewItem> items;
    private ImageUtils imageUtils;
    private static LayoutInflater inflater = null;

    public CustomAdapter(MainActivity mainActivity, int recourseId, List<ListViewItem> list) {
        super(mainActivity, recourseId, list);
        items = list;
        imageUtils = ImageUtils.getInstance(mainActivity);
        inflater = (LayoutInflater) mainActivity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        TextView tv;
        ImageView img;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.list_item, null);
        holder.tv = (TextView) rowView.findViewById(R.id.textView1);
        holder.img = (ImageView) rowView.findViewById(R.id.imageView1);
        holder.tv.setText(getItem(position).getText());
        holder.img.setImageBitmap(imageUtils.loadImage(getItem(position).getImage()));
        return rowView;
    }

}
